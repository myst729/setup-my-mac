# setup-my-mac

## mac os x
```bash
# Make a USB installer
sudo /Applications/Install\ macOS\ Sierra.app/Contents/Resources/createinstallmedia --volume /Volumes/MyVolume --applicationpath /Applications/Install\ macOS\ Sierra.app

# Disable autoboot
sudo nvram AutoBoot=%00

# Enable autoboot
sudo nvram AutoBoot=%03

# Show full path in Finder
defaults write com.apple.finder _FXShowPosixPathInTitle -bool YES

# Clear folder view
sudo find / -name ".DS_Store" -depth -exec rm {} \;
sudo chown -R $USER /usr/local

# Fix volume items being used
xattr -rd com.apple.FinderInfo /Volumes/Foo/

# Build soft links
ln -s /Applications/Sublime\ Text.app/Contents/SharedSupport/bin/subl ./subl
ln -s /Applications/Visual\ Studio\ Code.app/Contents/MacOS/Electron ./vsc
sudo chown $USER ./subl
sudo chown $USER ./vsc

# Customize hosts and paths
/etc/hosts
/etc/paths

# Install Xcode command line tools
xcode-select --install

# Allow apps downloaded from anywhare
sudo spctl --master-disable

# Add custom desktop and user pictures
/Library/Desktop Pictures/
/Library/User Pictures/
```

## apache
```bash
mkdir ~/Sites

sudo chmod 644 /etc/apache2/users/Leo.conf

# /etc/apache2/users/Leo.conf
<Directory "/Users/Leo/Sites/">
  AllowOverride All
  Options Indexes MultiViews FollowSymLinks
  Require all granted
</Directory>

# /etc/apache2/httpd.conf
LoadModule authz_host_module libexec/apache2/mod_authz_host.so
LoadModule authz_core_module libexec/apache2/mod_authz_core.so
LoadModule userdir_module libexec/apache2/mod_userdir.so
LoadModule vhost_alias_module libexec/apache2/mod_vhost_alias.so
LoadModule php5_module libexec/apache2/libphp5.so
Include /private/etc/apache2/extra/httpd-userdir.conf
Include /private/etc/apache2/extra/httpd-vhosts.conf

# /etc/apache2/extra/httpd-userdir.conf
Include /private/etc/apache2/users/*.conf

sudo apachectl start
sudo apachectl stop
sudo apachectl restart
```

## brew
```bash
ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

sudo chown -R $USER /usr/local/bin
sudo chown -R $USER /usr/local/share
```

## git
```bash
ssh-keygen -t rsa -C "you@personal.com"
ssh-keygen -t rsa -C "you@company.com"

ssh-add -D
ssh-add ~/.ssh/id_rsa_personal
ssh-add ~/.ssh/id_rsa_company

git config --global user.name "Your Name"
git config --global user.email "you@personal.com"

git config user.name "Your Name"
git config user.email "you@company.com"

ssh -T git@github.com
```

### ~/.ssh/config
```bash
#personal
Host github.com
  HostName github.com
  User git
  IdentityFile ~/.ssh/id_rsa_personal

Host gitlab.com
  HostName gitlab.com
  User git
  IdentityFile ~/.ssh/id_rsa_personal

Host bitbucket.org
  HostName bitbucket.org
  User git
  IdentityFile ~/.ssh/id_rsa_personal

#company
Host company
  HostName git.company.com
  User git
  IdentityFile ~/.ssh/id_rsa_company
```

## node
```bash
sudo chown -R $USER /usr/local/bin
sudo chown -R $USER /usr/local/share
sudo chown -R $USER /usr/local/lib/node_modules
```

## zsh
```bash
chsh -s /bin/zsh
sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
sh -c "$(wget https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh -O -)"
```
