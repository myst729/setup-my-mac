export PATH="/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin"
export JAVA_HOME="/Library/Java/JavaVirtualMachines/jdk-9.0.1.jdk/Contents/Home"
export ZSH=/Users/leo/.oh-my-zsh

ZSH_THEME="leo"
plugins=(autojump brew git osx)
source $ZSH/oh-my-zsh.sh


# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion. Case
# sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/rsa_id"


alias chrome="/Applications/Google\ Chrome.app/Contents/MacOS/Google\ Chrome"
alias chromebug="chrome --remote-debugging-port=9222"
alias chromeless="chrome --headless --disable-gpu"
alias cls="clear"
alias grep="grep --color=auto"
alias javac="javac -J-Dfile.encoding=utf8"
alias la='ls -a'
alias ll='ls -l'
alias lal='ls -al'
alias unrar="unrar x"

alias -s html=subl
alias -s xml=subl
alias -s svg=subl
alias -s md=subl
alias -s css=subl
alias -s sass=subl
alias -s scss=subl
alias -s styl=subl
alias -s js=subl
alias -s jsx=subl
alias -s ts=subl
alias -s json=subl
alias -s vue=subl
alias -s php=subl
alias -s py=subl
alias -s rb=subl
alias -s sql=subl
alias -s txt=subl
alias -s bz2='tar -xjvf'
alias -s gz='tar -xzvf'
alias -s rar='unrar x'
alias -s tgz='tar -xzvf'
alias -s zip='unzip'
